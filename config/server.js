module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: "http://localhost:1337",
  admin: {
    url:'/dashboard',
    auth: {
      secret: env('ADMIN_JWT_SECRET', '907faf2ebde9802aabd866b25368f670'),
    },
  },
});
